'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _reactRouter2 = _interopRequireDefault(_reactRouter);

var _RuxibleJs = require('./Ruxible.js');

var _RuxibleJs2 = _interopRequireDefault(_RuxibleJs);

var RuxibleBuilder = (function () {
    function RuxibleBuilder(rx) {
        _classCallCheck(this, RuxibleBuilder);

        this.rx = rx;
        this.currentPath = null;
        this.paths = [];
    }

    _createClass(RuxibleBuilder, [{
        key: 'path',
        value: function path(name) {
            this.currentPath = new Path(name, this.rx);
            return this;
        }
    }, {
        key: 'action',
        value: function action(_action, payload) {
            this.currentPath.action(_action, payload);
            return this;
        }
    }, {
        key: 'registerStore',
        value: function registerStore() {
            var stores = Array.prototype.slice.call(arguments);
            this.currentPath.addStore(stores);
            return this;
        }
    }, {
        key: 'commit',
        value: function commit() {
            this.paths[this.currentPath.name] = this.currentPath;
            this.currentPath = null;
            return this;
        }
    }, {
        key: 'build',
        value: function build(state) {
            var pathSize = Object.keys(this.paths).length;
            if (pathSize === 0) throw 'can not build without add path.';

            this.rx.setPath(this.getCurrentPath(state.path));
            return this.rx;
        }
    }, {
        key: 'getCurrentPath',
        value: function getCurrentPath(path) {
            var pathName = path === '/' ? Object.keys(this.paths)[0] : path;
            var currentPath = this.paths[pathName];
            if (currentPath) {
                return currentPath;
            } else {
                throw 'can not get path :' + path;
            }
        }
    }], [{
        key: 'create',
        value: function create(root) {
            if (!root) throw 'root can not be null.';
            return new RuxibleBuilder(new _RuxibleJs2['default'](root));
        }
    }]);

    return RuxibleBuilder;
})();

var Path = (function () {
    function Path(name, rx) {
        _classCallCheck(this, Path);

        if (!name) throw 'path can not be null.';

        this.name = name;
        this.rx = rx;
        this.actions = [];
        this.stores = [];
    }

    _createClass(Path, [{
        key: 'action',
        value: function action(_action2, payload) {
            var act = {
                action: _action2,
                payload: payload
            };

            this.actions.push(act);
        }
    }, {
        key: 'addStore',
        value: function addStore(stores) {
            this.stores = stores;
        }
    }]);

    return Path;
})();

module.exports = RuxibleBuilder;
//# sourceMappingURL=RuxibleBuilder.js.map