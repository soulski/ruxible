'use strict';

Object.defineProperty(exports, '__esModule', {
    value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _RuxibleContextJs = require('./RuxibleContext.js');

var _RuxibleContextJs2 = _interopRequireDefault(_RuxibleContextJs);

var _RuxibleComponentJs = require('./RuxibleComponent.js');

var _RuxibleComponentJs2 = _interopRequireDefault(_RuxibleComponentJs);

var _StoreContainerJs = require('./StoreContainer.js');

var _StoreContainerJs2 = _interopRequireDefault(_StoreContainerJs);

var _AppDispatcherJs = require('./AppDispatcher.js');

var _AppDispatcherJs2 = _interopRequireDefault(_AppDispatcherJs);

var Ruxible = (function () {
    function Ruxible(component) {
        _classCallCheck(this, Ruxible);

        this._component = component;
        this._storeContainer = new _StoreContainerJs2['default']();
        this._dispatcher = new _AppDispatcherJs2['default']();
        this._context = new _RuxibleContextJs2['default'](this._storeContainer, this._dispatcher);
    }

    _createClass(Ruxible, [{
        key: 'createComponent',
        value: function createComponent() {
            return _react2['default'].createElement(
                _RuxibleComponentJs2['default'],
                { context: this._context.getComponentContext() },
                _react2['default'].createElement(this._component, null)
            );
        }
    }, {
        key: 'registerStore',
        value: function registerStore(storeClass) {
            this._storeContainer.registerStore(storeClass);
            this._dispatcher.register((function (store, events) {
                return function (action) {
                    for (var _len = arguments.length, payload = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
                        payload[_key - 1] = arguments[_key];
                    }

                    if (events[action]) {
                        var _store$handler;

                        var handler = events[action];
                        (_store$handler = store[handler]).call.apply(_store$handler, [store].concat(payload));
                    }
                };
            })(this._storeContainer.getStore(storeClass), storeClass.events));
        }
    }, {
        key: 'getContext',
        value: function getContext() {
            return this._context;
        }
    }, {
        key: 'getActionContext',
        value: function getActionContext() {
            return this._context.getActionContext();
        }
    }, {
        key: 'getComponentContext',
        value: function getComponentContext() {
            return this._context.getComponentContext();
        }
    }, {
        key: 'serialize',
        value: function serialize() {
            return {
                stores: this._storeContainer.serialize(),
                context: this._context.serialize()
            };
        }
    }, {
        key: 'deserialize',
        value: function deserialize(obj) {
            this._storeContainer.deserialize(obj.stores);
            this._context.deserialize(obj.context);
        }
    }, {
        key: 'setPath',
        value: function setPath(path) {
            this.path = path;
        }
    }, {
        key: 'render',
        value: function render(res, document) {
            if (res && document) {
                var rawContext = JSON.parse(document.getElementById('context').textContent);

                this.deserialize(rawContext);
                this._registerStores();

                this._clientRender(res);
            } else if (res) {
                this._registerStores();
                if (this.path.actions.length > 0) {
                    this._doAction(res, 0);
                } else {
                    this._serverRender(res);
                }
            } else {
                throw 'Invalid arguments';
            }
        }
    }, {
        key: '_doAction',
        value: function _doAction(res, counter) {
            var _this = this;

            var len = this.path.actions.length;
            var act = this.path.actions[counter];

            var promise = this.getActionContext().executeAction(act.action, act.payload);
            promise.then(function () {
                counter = counter + 1;
                if (counter < len) {
                    _this._doAction(res, counter);
                } else {
                    _this._finish(res);
                }
            })['catch'](function (err) {
                console.log(err);
            });
        }
    }, {
        key: '_registerStores',
        value: function _registerStores() {
            var _this2 = this;

            this.path.stores.forEach(function (store) {
                _this2.registerStore(store);
            });
        }
    }, {
        key: '_serverRender',
        value: function _serverRender(res) {
            res.render('main', {
                content: _react2['default'].renderToString(this.createComponent()),
                context: this.serialize()
            });
        }
    }, {
        key: '_clientRender',
        value: function _clientRender(res) {
            res.render(this.createComponent(), document.getElementById('content'));
        }
    }]);

    return Ruxible;
})();

exports['default'] = Ruxible;
module.exports = exports['default'];
//# sourceMappingURL=Ruxible.js.map