var gulp = require('gulp'),
    path = require('path'),
    del = require('del'),
    babelRegister = require('babel/register');

var babel = require('gulp-babel'),
    concat = require('gulp-concat'),
    clean = require('gulp-clean'),
    rename = require('gulp-rename'),
    sourcemaps = require('gulp-sourcemaps'),
    mocha = require('gulp-mocha'),
    plumber = require('gulp-plumber');

var paths = {
    src: ['src/**/*.js'],
    dest: 'lib/',
    test: ['test/**/*.js'],

    sourceRoot: path.join(__dirname, 'src/')
};

gulp.task('es6', function() {
    return gulp.src(paths.src)
        .pipe(plumber())
        .pipe(sourcemaps.init())
            .pipe(babel())
        .pipe(sourcemaps.write('.', {sourceRoot: paths.sourceRoot}))
        .pipe(gulp.dest(paths.dest));
});

gulp.task('clean', function(done) {
    del([
        paths.dest + '**/*',
    ], done);
});

gulp.task('watch', function() {
    gulp.watch(paths.src, ['es6']);
});

gulp.task('test', function() {
    return gulp.src(paths.test)
        .pipe(mocha({
            reporter: 'nyan',
            compilers: { js: babelRegister }
        }));
});

gulp.task('default', ['clean', 'es6', 'watch']);
