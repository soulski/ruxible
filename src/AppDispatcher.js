import {Dispatcher} from 'flux';

class AppDispatcher {

    constructor() {
        this._dispatcher = new Dispatcher();
    }

    register(cb) {
        this._dispatcher.register(payload => {
            payload = JSON.parse(JSON.stringify(payload));
            if (payload.__type__ === 'mutiArgs') {
                Array.prototype.unshift.call(payload.arguments, payload.action);
                cb.apply(null, payload.arguments);
            } else {
                cb(payload);
            }
        });
    }

    dispatch() {
        if (arguments.length <= 1) {
            this._dispatcher.dispatch(arguments);
        } else {
            let args = Array.prototype.slice.call(arguments, 1);
            this._dispatcher.dispatch({
                __type__: 'mutiArgs',
                action: arguments[0],
                arguments: args
            });
        }
    }

}

export default AppDispatcher;
