class RuxibleContext {

    constructor(storeContainer, dispatcher) {
        this._storeContainer = storeContainer;
        this._dispatcher = dispatcher;
    }

    getContext() {
        return {
            executeAction: this.asyncExecuteAction.bind(this)
        };
    }

    getActionContext() {
        return {
            executeAction: this.asyncExecuteAction.bind(this),
            getStore: this._storeContainer.getStore.bind(this._storeContainer),
            dispatch: this._dispatcher.dispatch.bind(this._dispatcher)
        };
    }

    getComponentContext() {
        return {
            executeAction: this.syncExecuteAction.bind(this),
            getStore: this._storeContainer.getStore.bind(this._storeContainer)
        };
    }

    asyncExecuteAction(action, payload, callback) {
        let promise = this._executeAction(this.getActionContext(), action, payload);
        if (callback && callback instanceof Function) {
            promise.then((result) => {
                callback(null, result);
            }, (err) => {
                callback(err, null);
            });

            return;
        }

        return promise;
    }

    syncExecuteAction(action, payload) {
        return this._executeAction(this.getActionContext(), action, payload);
    }

    _executeAction(context, action, payload) {
        let promise = new Promise((resolve, reject) => {
            let pAction = action(context, payload, (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });

            if (pAction instanceof Promise) {
                pAction.then(resolve, reject);
            }
        });

        return promise;
    }

    serialize() {
        return {
            stores: this._storeContainer.serialize(),
        }
    }

    deserialize(obj) {
        this._storeContainer.deserialize(obj.stores);
    }

}

export default RuxibleContext;