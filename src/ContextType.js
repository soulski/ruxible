import React from 'react';

export default {
    getStoreType: React.PropTypes.func.isRequired,
    executeActionType: React.PropTypes.func.isRequired
}
