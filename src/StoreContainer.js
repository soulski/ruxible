import BaseStore from './BaseStore.js';

export default class StoreContainer {

    constructor() {
        this._stores = {};
        this._rawStores = {};
    }

    registerStore(storeClass) {
        let rawStores = this._rawStores;
        let stores = this._stores;
        let storeName = storeClass.storeName;
        let store = new storeClass();

        if (rawStores[storeName]) {
            store.deserialize(rawStores[storeName]);
        }

        this._stores[storeName] = store;
    }

    getStore(storeClass) {
        let storeName = storeClass.storeName || storeClass;
        let store = this._stores[storeName];
        let rawStore = this._rawStores[storeName];

        if (!store && rawStore) {
            this.registerStore(storeClass);
            store = this._stores[storeName];
        }

        return store;
    }

    serialize() {
        var context = {
            stores: {}
        };

        var stores = this._stores;
        for (var key in stores) {
            if (stores.hasOwnProperty(key)) {
                var store = this.getStore(key);
                context.stores[key] = store.serialize();
            }
        }

        return context;
    }

    deserialize(context) {
        var rawStores = this._rawStores;

        var stores = context.stores;
        for (var key in stores) {
            if (stores.hasOwnProperty(key)) {
                rawStores[key] = stores[key];
            }
        }
    }

}
