import React from 'react';
import cloneWithProps from 'react/addons/cloneWithProps';
import { getStoreType, executeActionType } from './ContextType.js';

class RuxibleComponent extends React.Component {

    constructor(props) {
        super(props);
    }

    getChildContext() {
        return {
            getStore: this.props.context.getStore,
            executeAction: this.props.context.executeAction
        };
    }

    render() {
        return cloneWithProps(this.props.children, {
            context: this.props.context
        });
    }

}

RuxibleComponent.propTypes = {
    context: React.PropTypes.object.isRequired
};
RuxibleComponent.childContextTypes = {
    getStore: getStoreType,
    executeAction: executeActionType
};

export default RuxibleComponent;
