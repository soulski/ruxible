import React from 'react';
import Router from 'react-router';
import Ruxible from './Ruxible.js';

class RuxibleBuilder {
    static create(root) {
        if(!root) throw 'root can not be null.';
        return new RuxibleBuilder(new Ruxible(root));
    }

    constructor(rx) {
        this.rx = rx;
        this.currentPath = null;
        this.paths = [];
    }

    path(name) {
        this.currentPath = new Path(name, this.rx);
        return this;
    }

    action(action, payload) {
        this.currentPath.action(action, payload);
        return this;
    }

    registerStore() {
        let stores = Array.prototype.slice.call(arguments);
        this.currentPath.addStore(stores);
        return this;
    }

    commit() {
        this.paths[this.currentPath.name] = this.currentPath;
        this.currentPath = null;
        return this;
    }

    build(state) {
        let pathSize = Object.keys(this.paths).length;
        if(pathSize === 0) throw 'can not build without add path.';

        this.rx.setPath(this.getCurrentPath(state.path));
        return this.rx;
    }

    getCurrentPath(path) {
        let pathName = path === '/' ? Object.keys(this.paths)[0] : path;
        let currentPath = this.paths[pathName];
        if(currentPath) {
            return currentPath;
        }
        else{
            throw 'can not get path :' + path;
        }
    }
}

class Path {
    constructor(name, rx) {
        if(!name) throw 'path can not be null.';

        this.name = name;
        this.rx = rx;
        this.actions = [];
        this.stores = [];
    }

    action(action, payload) {
        let act = {
            action: action,
            payload: payload
        };

        this.actions.push(act);
    }

    addStore(stores) {
        this.stores = stores;
    }
}
module.exports = RuxibleBuilder;