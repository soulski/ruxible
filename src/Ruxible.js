import React from 'react';

import RuxibleContext from './RuxibleContext.js';
import RuxibleComponent from './RuxibleComponent.js';
import StoreContainer from './StoreContainer.js';
import AppDispatcher from './AppDispatcher.js';

export default class Ruxible {

    constructor(component) {
        this._component = component;
        this._storeContainer = new StoreContainer();
        this._dispatcher = new AppDispatcher();
        this._context = new RuxibleContext(this._storeContainer, this._dispatcher);
    }

    createComponent() {
        return (
            <RuxibleComponent context={this._context.getComponentContext()}>
                <this._component/>
            </RuxibleComponent>
        );
    }

    registerStore(storeClass) {
        this._storeContainer.registerStore(storeClass);
        this._dispatcher.register((store, events) => {
            return function (action, ...payload) {
                if (events[action]) {
                    var handler = events[action];
                    store[handler].call(store, ...payload);
                }
            }
        } (this._storeContainer.getStore(storeClass), storeClass.events));
    }

    getContext() {
        return this._context;
    }

    getActionContext() {
        return this._context.getActionContext();
    }

    getComponentContext() {
        return this._context.getComponentContext();
    }

    serialize() {
        return {
            stores: this._storeContainer.serialize(),
            context: this._context.serialize()
        }
    }

    deserialize(obj) {
        this._storeContainer.deserialize(obj.stores);
        this._context.deserialize(obj.context);
    }

    setPath(path) {
        this.path = path;
    }

    render(res, document) {
        if(res && document){
            let rawContext = JSON.parse(document.getElementById('context').textContent);

            this.deserialize(rawContext);
            this._registerStores();

            this._clientRender(res);
        }
        else if(res) {
            this._registerStores();
            if(this.path.actions.length > 0) {
                this._doAction(res, 0);
            }
            else{
                this._serverRender(res);
            }
        }
        else {
            throw 'Invalid arguments';
        }
    }

    _doAction(res, counter) {
        let len = this.path.actions.length;
        let act = this.path.actions[counter];

        let promise = this.getActionContext().executeAction(act.action, act.payload);
        promise.then(() => {
                counter = counter + 1;
                if(counter < len){
                    this._doAction(res, counter);
                }
                else{
                    this._finish(res);
                }
            })
            .catch(err => {
                console.log(err);
            });
    }

    _registerStores() {
        this.path.stores.forEach(store => {
            this.registerStore(store);
        });
    }

    _serverRender(res) {
        res.render('main', {
            content: React.renderToString(this.createComponent()),
            context: this.serialize()
        });
    }

    _clientRender(res) {
        res.render(this.createComponent(), document.getElementById('content'));
    }
}
