import should from 'should';
import React from 'react';
import TestUtils from 'react/addons/TestUtils';
import Ruxible from '../src/Ruxible.js';
import MockStore from './mock/MockStore.js';
import { jsdom } from 'jsdom';

describe('Ruxible', () => {

    before((done) => {
        jsdom.env('<html><body></body></html>', [], (err, window) => {
            global.window = window;
            global.document = window.document;
            global.navigator = window.navigator;

            done();
        });
    });

    after(() => {
        delete global.document;
        delete global.window;
    });

    describe('#constructor', () => {

        it('with invalid-option should not throw error', () => {
            (() => {
                let ruxible = new Ruxible();
                ruxible.should.be.ok;

                ruxible = new Ruxible({
                    unknow: 'test'
                });
                ruxible.should.be.ok;
            }).should.not.throw();
        });

        it('with fully options should be success', () => {
            let ruxible = new Ruxible(React.createClass({
                render: function() {

                }
            }));

            ruxible.should.be.ok;
        });
    });

    describe('#registerStore', () => {

        it('with store should be success', (done) => {
            let ruxible = new Ruxible();
            let msg = 'dispatch';

            MockStore.prototype.testEvent = (payload) => {
                payload.should.be.ok;
                payload.should.be.String;
                payload.should.be.eql(msg);
                done();
            }

            ruxible.registerStore(MockStore);
            ruxible._dispatcher.dispatch('TEST_EVENT', msg);
        });

        it('with non-store should be unsuccess', () => {
            (() => {
                let ruxible = new Ruxible();
                ruxible.registerStore({});
            }).should.throw();
        });

    });

    describe('#createComponent', () => {

        it('with div component should success', () => {
            const MockComponent = React.createClass({

                render: function() {
                    return <div className="Component"></div>;
                }

            });

            let ruxible = new Ruxible(MockComponent);
            const component = ruxible.createComponent();
            const componentContext = component.props.context;
            const html = React.renderToStaticMarkup(component);

            html.should.match('<div class="Component"></div>');

            componentContext.should.be.eql(ruxible.getComponentContext());
        });

    });

    describe('#serialize', () => {

        it('should be have store and context', () => {
            let ruxibleSrc = new Ruxible();
            ruxibleSrc.registerStore(MockStore);

            let serialize = ruxibleSrc.serialize();
            serialize.stores.should.be.ok;
            serialize.context.should.be.ok;
        });

    }); 
});
