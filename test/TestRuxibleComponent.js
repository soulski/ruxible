import React from 'react';
import TestUtils from 'react/addons/TestUtils';
import RuxibleComponent from '../src/RuxibleComponent.js';
import { getStoreType, executeActionType } from '../src/ContextType.js';
import { jsdom } from 'jsdom';

describe('RuxibleComponent', () => {

    before(() => {
        global.document = jsdom('<html><header></header><body></body></html>');
        global.window = document.parentWindow;
    });

    describe('#render', () => {

        it('should pass context to children', (done) => {
            let mockContext = {
                getStore: () => {},
                executeAction: () => {}
            }

            class StubComponent extends React.Component {

                render() {
                    this.context.getStore.should.be.ok;
                    this.context.executeAction.should.be.ok;
                    done();

                    return <div></div>;
                }

            }

            StubComponent.contextTypes = {
                getStore: getStoreType,
                executeAction: executeActionType
            };

            TestUtils.renderIntoDocument(
                <RuxibleComponent context={mockContext}><StubComponent/></RuxibleComponent>
            );
        });

    });

});
