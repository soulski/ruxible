import should from 'should';
import React from 'react';
import RuxibleBuilder from '../src/RuxibleBuilder.js';
import MockStore from './mock/MockStore.js';

describe('RuxibleBuilder', () => {

    const MockComponent = React.createClass({
        render: function() {
            return <div className="Component"></div>;
        }
    });

    describe('#createBuilder', () => {

        it('wite component should be success', () => {
            let builder = RuxibleBuilder.create(MockComponent);
            builder.should.be.ok;
        });

        it('with non-component should be unsuccess', () => {
            (() => {
                RuxibleBuilder.create();
            }).should.throw();
        });

    });

    describe('#addPath', () => {

        it('with non-path should be unsuccess', () => {
            (() => {
                RuxibleBuilder.create(MockComponent)
                    .path();
            }).should.throw();
        });

        it('after commit should be add path to builder', () => {
            let builder = RuxibleBuilder.create(MockComponent)
                        .path('/aaa')
                        .commit();

            builder.should.be.ok;
            builder.should.be.an.Object;
            builder.paths['/aaa'].should.be.ok;
        });

        it('add action should be return right action', () => {
            let mockPayload = {};
            let mockAction = (context, payload) => {};
            let expectAction = [
            {
                action: mockAction,
                payload: mockPayload
            },
            {
                action: mockAction,
                payload: mockPayload
            }]

            let builder = RuxibleBuilder.create(MockComponent)
                        .path('/aaa')
                            .action(mockAction, mockPayload)
                            .action(mockAction, mockPayload)
                        .commit();

            builder.should.be.ok;
            builder.should.be.an.Object;
            builder.paths['/aaa'].actions.should.eql(expectAction);
        });

        it('register store should be return right store', () => {
            let expectAction = [MockStore, MockStore];
            let builder = RuxibleBuilder.create(MockComponent)
                        .path('/aaa')
                            .registerStore(MockStore, MockStore)
                        .commit();

            builder.should.be.ok;
            builder.should.be.an.Object;
            builder.paths['/aaa'].stores.should.eql(expectAction);
        });

    });

    describe('#buildBuilder', () => {

        let state = {
            path: '/aaa'
        }

        it('build without path should be unsuccess', () => {
            (() => {
                RuxibleBuilder.create(MockComponent)
                    .build(state);
            }).should.throw();
        });

        it('add path without commit should be unsuccess', () => {
            (() => {
                RuxibleBuilder.create(MockComponent)
                    .path('/aaa')
                    .build(state);
            }).should.throw();
        });

        it('build should be success', () => {
            let builder = RuxibleBuilder.create(MockComponent)
                            .path('/aaa')
                                .commit()
                            .path('/bbb')
                                .commit()
                            .build(state);

            builder.should.be.ok;
            builder.should.be.an.Object;

            let MockResponse = {
                render : () => {}
            };

            builder.render(MockResponse);
        });

    });

});
