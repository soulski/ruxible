import should from 'should';
import React from 'react';
import StoreContainer from '../src/StoreContainer.js';
import MockStore from './mock/MockStore.js';

describe('StoreContainer', () => {

    describe('#registerStore', () => {

        it('with store should be success', () => {
            let container = new StoreContainer();
            container.registerStore(MockStore);

            let store = container.getStore(MockStore);
            store.should.be.ok;
            store.name.should.eql(MockStore.storeName);
        });

        it('with non-store should be unsuccess', () => {
            (() => {
                let ruxible = new Ruxible();
                ruxible.registerStore({});
            }).should.throw();
        });

    });

    describe('#getStore', () => {

        it('should return right store', () => {
            let container = new StoreContainer();
            container.registerStore(MockStore);

            let store = container.getStore(MockStore);
            store.should.be.ok;
            store.name.should.eql(MockStore.storeName);

            let transitContainer = new StoreContainer();
            transitContainer.deserialize(container.serialize());

            store = transitContainer.getStore(MockStore);
            store.should.be.ok;
            store.name.should.eql(MockStore.storeName);
        });

    })

    describe('#serialize and deserialize', () => {

        it('should be able to serialize and deserialize with complete result', () => {
            let srcContainer = new StoreContainer();
            srcContainer.registerStore(MockStore);

            let srcStore = srcContainer.getStore(MockStore);
            srcStore.addObj({name: 'test'});

            let dstContainer = new StoreContainer();
            dstContainer.deserialize(srcContainer.serialize());

            let dstStore = dstContainer.getStore(MockStore);
            dstStore.should.be.ok;
            dstStore.name.should.be.eql(MockStore.storeName);

            let resultList = dstStore.getList();
            resultList.length.should.be.equal(1);
            resultList[0].name.should.be.equal('test');
        });

    });

});

